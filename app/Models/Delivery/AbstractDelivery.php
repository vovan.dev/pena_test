<?php

namespace App\Models\Delivery;

use App\Models\Cargo;
use Illuminate\Database\Eloquent\Model;

abstract class AbstractDelivery extends Model
{

    public function cargo ()
    {
        return $this->morphOne(Cargo::class, 'delivery');
    }

    abstract public function calculate ();

}
