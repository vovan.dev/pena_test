<?php

namespace App\Models\Delivery;

use App\Models\Delivery\AbstractDelivery;

class DeliveryRussianPost extends AbstractDelivery
{

    public $table = 'delivery_russian_post';

    public function calculate ()
    {
        return intval($this->cargo->weight) > 10 ? 1000 : 100;
    }

}