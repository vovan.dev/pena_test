<?php

namespace App\Models;

use App\Models\Delivery\AbstractDelivery;

class DeliveryDHL extends AbstractDelivery
{

    public $table = 'delivery_dhl';

    public function calculate ()
    {
        return intval($this->cargo->weight) * 100;
    }

}
