<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cargo extends Model
{

    public $fillable = ['name', 'weight'];
    public $table = 'cargo';

    public function delivery ()
    {
        return $this->morphTo();
    }

}
